﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCSpawner : MonoBehaviour {

    [SerializeField]
    GameObject spawnPosObj;
    [SerializeField]
    GameObject NPCPrefab;

    [SerializeField]
    LayerMask spawnFloorMask;

    void Start () {
        if(PhotonNetwork.isMasterClient)
            Spawn();

        Avelog.Input.PressAttackEvent += Spawn;
    }

    void Spawn()
    {
        RaycastHit hit;
        if (Physics.Raycast(spawnPosObj.transform.position, Vector3.down, out hit, 100f, spawnFloorMask))
        {
            PhotonNetwork.InstantiateSceneObject(NPCPrefab.name, hit.point, spawnPosObj.transform.rotation, 0, null);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.P))
        {
            Spawn();
        }
    }

}
