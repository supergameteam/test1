﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NPCBrain : Photon.PunBehaviour {

    NavMeshAgent navAgent;
    List<GameObject> Targets {
        get
        {
            return PlayerSpawner.playersGO;
        }
    }

    bool isInited = false;

	IEnumerator Start () {
        navAgent = GetComponent<NavMeshAgent>();

        while (Targets == null || Targets.Count == 0)
        {
            yield return null;
        }
        Init();
	}

    void Init()
    {
        isInited = true;
    }

    void Update () {
        if (!photonView.isMine || !isInited)
            return;

        GameObject target = GetNearestTarget();

        navAgent.destination = target.transform.position;
	}

    GameObject GetNearestTarget()
    {
        GameObject curTarget = Targets[0];
        float curDist = Vector3.Distance(transform.position, curTarget.transform.position);

        foreach(var target in Targets)
        {
            if (target == curTarget)
                continue;

            float dist = Vector3.Distance(transform.position, target.transform.position);

            if (dist < curDist)
                curTarget = target;
        }

        return curTarget;
    }

}
