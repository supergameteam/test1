﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : Photon.PunBehaviour {

    [SerializeField]
    float speed = 1f;
    [SerializeField]
    float damage = 10f;

    [SerializeField]
    string[] HitableMask = { "Terrain", "Player" };
    int hitableMask;

    void Start () {
        foreach (var maskLayer in HitableMask)
        {
            hitableMask = hitableMask | LayerMask.NameToLayer(maskLayer);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!photonView.isMine)
            return;

        if((other.gameObject.layer & hitableMask) != 0)
        {
            Blow(other);
        }
    }

    void Blow(Collider hitObj)
    {
        Health health = hitObj.GetComponentInParent<Health>();
        if (health != null)
        {
            health.ChangeHealth(-damage);
        }
        //PhotonNetwork.Destroy(gameObject);
    }

    void Update () {

        if (!photonView.isMine)
            return;

        transform.position = transform.position + transform.forward * speed * Time.deltaTime;
    }


}
