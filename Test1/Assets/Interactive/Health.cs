﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Health : Photon.PunBehaviour, IPunObservable {

    [SerializeField]
    float maxHP = 100f;
    public float MaxHP
    {
        get
        {
            return maxHP;
        }
    }
    [SerializeField]
    float curHP = 100f;
    public float CurHP
    {
        get
        {
            return curHP;
        }
    }

    public event System.Action ChangeHealthEvent;

    void Start() {

    }

    void Update() {

    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            // We own this player: send the others our data
            stream.SendNext(curHP);
        }
        else
        {
            // Network player, receive data
            this.curHP = (float)stream.ReceiveNext();
            if (ChangeHealthEvent != null)
            {
                ChangeHealthEvent();
            }
        }
    }

    public void ChangeHealth(float value)
    {
        photonView.RPC("ChangeHealthRPC", PhotonTargets.All, value);
    }

    [PunRPC]
    void ChangeHealthRPC(float value)
    {
        if (!photonView.isMine)
            return;

        ChangeHealthLocal(value);
    }

    void ChangeHealthLocal(float value)
    {
        curHP = Mathf.Clamp(curHP + value, 0f, maxHP);
        Debug.Log("CurHealth = " + curHP);
    }

}
