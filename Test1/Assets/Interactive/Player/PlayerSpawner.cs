﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : Photon.PunBehaviour
{
    [SerializeField]
    GameObject spawnPosObj;
    [SerializeField]
    GameObject playerPrefab;

    public static event System.Action<GameObject> LocalPlayerSpawnEvent;
    public static GameObject LocalPlayerGO { get; private set; }

    public static event System.Action<GameObject> PlayerSpawnEvent;
    public static List<GameObject> playersGO = new List<GameObject>();
    
	void Start () {
        SpawnLocalPlayer();
    }

    void SpawnLocalPlayer()
    {
        LocalPlayerGO = PhotonNetwork.Instantiate(playerPrefab.name, spawnPosObj.transform.position, spawnPosObj.transform.rotation, 0);
        if (LocalPlayerSpawnEvent != null)
        {
            LocalPlayerSpawnEvent(LocalPlayerGO);
        }
        //AddPlayerToList(LocalPlayerGO);
    }

    public static void AddPlayerToList(GameObject playerGO)
    {
        playersGO.Add(playerGO);
        if (PlayerSpawnEvent != null)
        {
            PlayerSpawnEvent(playerGO);
        }
    }

}
