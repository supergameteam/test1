﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour {

    Camera cam;
    [SerializeField]
    PhotonView playerPhotonView;

    private void Start()
    {
        cam = GetComponent<Camera>();
        SetupCamera();
    }

    void SetupCamera()
    {
        bool isLocalPlayer = playerPhotonView == null || playerPhotonView != null && playerPhotonView.isMine;

        if (isLocalPlayer)
        {
            cam.enabled = true;
        }
        else
        {
            cam.enabled = false;
        }
    }
}
