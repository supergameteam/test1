﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : Photon.PunBehaviour {

    CharacterController _charController;
    Camera _camera;

    [SerializeField]
    float moveSpeed = 10;
    [SerializeField]
    float rotSpeed = 300f;

    [SerializeField]
    float gravity = -0.9f;
    [SerializeField]
    string[] GroundMask = { "Terrain" };
    int groundMask;
    float curVertSpeed;
    [SerializeField]
    float maxFallSpeed;
    bool isGrounded = false;

    Vector3 curEul;

    bool isInited = false;

    void Awake()
    {
        _charController = GetComponent<CharacterController>();
        _charController.detectCollisions = true;
        _camera = GetComponentInChildren<Camera>(true);
        if (_charController == null)
        {
            Debug.LogError("Null ref: PlayerMove Awake");
        }
        foreach (var maskLayer in GroundMask)
        {
            groundMask = groundMask | (1 << LayerMask.NameToLayer(maskLayer));
        }

        curEul = transform.eulerAngles;

        isInited = true;
    }

    private void Update()
    {
        if (!isInited || !photonView.isMine)
            return;

        Move(Avelog.Input.JoystickInput);
        RotateBodyHorizontal(Avelog.Input.touchDeltaX);
        RotateHeadVertical(Avelog.Input.touchDeltaY);
        DetectGround();
        Fall();
    }

    void Move(float[] moveInput)
    {
        var horInput = Input.GetAxis("Horizontal") + moveInput[0];
        var forwInput = Input.GetAxis("Vertical") + moveInput[1];
        // Бежит вперед
        if (forwInput != 0 || horInput != 0)
        {
            forwInput *= moveSpeed * Time.deltaTime;
            horInput *= moveSpeed * Time.deltaTime;
            Vector3 moveDelta = transform.TransformDirection(new Vector3(horInput, 0, forwInput));

            _charController.Move(moveDelta);

        }
    }

    void RotateBodyHorizontal(float angle)
    {
        transform.Rotate(Vector3.up, angle, Space.World);
    }

    void RotateHeadVertical(float angle)
    {
        angle = Mathf.Clamp(angle + curEul.x, -90f, 90f) - curEul.x;
        _camera.transform.Rotate(transform.right, -angle, Space.World);
        curEul.x += angle;
    }

    void DetectGround()
    {
        RaycastHit hit;
        isGrounded = Physics.Raycast(transform.position, -transform.up, out hit, _charController.radius * 2f, groundMask);
    }

    void Fall()
    {
        if (isGrounded)
        {
            curVertSpeed = 0;
        }
        else
        {
            curVertSpeed = Mathf.Clamp(curVertSpeed + gravity * Time.deltaTime, maxFallSpeed, curVertSpeed);
            _charController.Move(transform.up * curVertSpeed * Time.deltaTime);

        }
    }

}
