﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShooter : Photon.PunBehaviour
{
    bool isInited = false;

    [SerializeField]
    float fireCooldown = 0f;
    [SerializeField]
    float fireInterval = 0.3f;

    [SerializeField]
    GameObject bulletPrefab;
    [SerializeField]
    float spawnDistFromCam = 1.5f;

    [SerializeField]
    GameObject cameraObj;

    void Start () {
        SetupShooter();
        isInited = true;
	}
    
    void SetupShooter()
    {
        if (!photonView.isMine)
            return;
       // Avelog.Input.PressAttackEvent += TryFire;
    }

    void Update () {
        if (!isInited || !photonView.isMine)
            return;

        if(fireCooldown < fireInterval)
            fireCooldown += Time.deltaTime;

        if(Input.GetKeyDown(KeyCode.V))
        {
            TryFire();
        }
    }

    void TryFire()
    {
        if (fireCooldown >= fireInterval)
        {
            fireCooldown = 0f;
            Fire();
        }
    }

    void Fire()
    {
        GameObject bulletGO = PhotonNetwork.Instantiate(bulletPrefab.name, cameraObj.transform.position + cameraObj.transform.forward * spawnDistFromCam, cameraObj.transform.rotation, 0);
        //Bullet bullet = bulletGO.GetComponent<Bullet>();
        //photonView.ObservedComponents.Add(bullet);
    }
}
