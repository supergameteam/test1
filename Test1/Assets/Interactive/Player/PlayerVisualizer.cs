﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVisualizer : Photon.PunBehaviour {

    [SerializeField]
    List<Renderer> myBodyRends;

	void Start () {
        SetupPlayerBodyVisibility();
    }

    void SetupPlayerBodyVisibility()
    {
        if (!photonView.isMine)
        {
            foreach(var renderer in myBodyRends)
            {
                renderer.enabled = true;
            }
        }
        else
        {
            foreach (var renderer in myBodyRends)
            {
                renderer.enabled = false;
            }
        }
    }

}
