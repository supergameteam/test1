﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Avelog
{
    public class Input
    {
        public static bool isAttackPressed;
        public static bool isMouseOverJoystick;
        static float[] joystickInput;
        public static float[] JoystickInput
        {
            get {
                if(joystickInput == null)
                {
                    joystickInput = new float[2];
                }
                return joystickInput;
            }
            private set { }
        }
        static List<int> blockedFingers;
        public static List<int> BlockedFingers {
            get {
                if (blockedFingers == null)
                {
                    blockedFingers = new List<int>();
                }
                return blockedFingers;
            }
            private set { }
        }
        public static float touchDeltaX;
        public static float touchDeltaY;

        public static event System.Action PressAttackEvent;
        public static void FirePressAttackEvent()
        {
            if(PressAttackEvent != null)
            {
                PressAttackEvent();
            }
        }
    }
}