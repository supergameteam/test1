﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Launcher : Photon.PunBehaviour
{

    [SerializeField]
    string gameVersion = "1";
    [SerializeField]
    PhotonLogLevel Loglevel = PhotonLogLevel.Informational;
    [SerializeField, Tooltip("The maximum number of players per room. When a room is full, it can't be joined by new players, and so new room will be created")]
    byte maxPlayersPerRoom = 2;
    [SerializeField]
    int sendRate = 10;

    void Awake()
    {
        PhotonNetwork.autoJoinLobby = false;
        //this makes sure we can use PhotonNetwork.LoadLevel() on the master client and all clients in the same room sync their level automatically
        PhotonNetwork.automaticallySyncScene = true;
        // Force LogLevel
        PhotonNetwork.logLevel = Loglevel;

        PhotonNetwork.ConnectUsingSettings(gameVersion);
    }

    public override void OnConnectedToMaster()
    {
        PhotonNetwork.JoinRandomRoom();
        Debug.Log("DemoAnimator/Launcher: OnConnectedToMaster() was called by PUN");
    }

    public override void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("DemoAnimator/Launcher: OnDisconnectedFromPhoton() was called by PUN");
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        Debug.Log("DemoAnimator/Launcher:OnPhotonRandomJoinFailed() was called by PUN. No random room available, so we create one.\nCalling: PhotonNetwork.CreateRoom(null, new RoomOptions() {maxPlayers = 4}, null);");
        PhotonNetwork.CreateRoom(null, new RoomOptions() { MaxPlayers = maxPlayersPerRoom }, null);
    }

    public override void OnJoinedRoom()
    {
        Debug.Log("DemoAnimator/Launcher: OnJoinedRoom() called by PUN. Now this client is in a room.");
        PhotonNetwork.LoadLevel("Main");
    }

}
