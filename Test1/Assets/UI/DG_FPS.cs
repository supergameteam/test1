﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;

public class DG_FPS : MonoBehaviour {

    float deltaTime = 0.0f;

    float cooldown;

    float msec;
    public float fps;

    Text fps_text;

	public bool debug = true;



    void Start()
    {
		if(debug)
       		fps_text = GetComponentInChildren<Text>();
    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        cooldown -= Time.deltaTime;

        if (cooldown <= 0)
        {
            fps = 1.0f / deltaTime;
			if(debug)
            	fps_text.text = fps.ToString();
            cooldown = 1;
        }

    }

}
