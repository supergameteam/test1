﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthBar : MonoBehaviour {

    PhotonView playerPhotonView;
    [SerializeField]
    Health health;
    [SerializeField]
    Image barImg;
    [SerializeField]
    GameObject barPanel;

	void Start () {
        playerPhotonView = PhotonView.Get(health);
        SetupBar();
    }

    void SetupBar()
    {
        if(playerPhotonView.isMine)
        {
            barPanel.SetActive(false);
        }
        else
        {
            barPanel.SetActive(true);
            health.ChangeHealthEvent += UpdateBar;
            UpdateBar();
        }
    }

    void UpdateBar()
    {
        barImg.fillAmount = health.CurHP / health.MaxHP;
    }
	
}


