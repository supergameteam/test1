﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickCreator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    [SerializeField]
    JoystickScript joystick;



    public void OnPointerDown(PointerEventData eventData)
    {
        joystick.ChangePos(eventData.position);
        joystick.gameObject.SetActive(true);
        joystick.PointerDown(eventData);
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        joystick.PointerUp(eventData);
        joystick.gameObject.SetActive(false);
    }

    public void OnDrag(PointerEventData eventData)
    {
        joystick.OnDrag(eventData);
    }
}
