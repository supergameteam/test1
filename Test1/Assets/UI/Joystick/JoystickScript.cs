﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

/* 12.06.16
   база для джойстика
   можешь написать свой опираясь на этот скрипт
   я его чуть изменил но не проверял так что могут быть ошибки, поэтому тебе придется разобраться как тут все работает
   тут все просто
   единственное шо есть precious делает так шоб ввод всевремя не посылался при малейшем рывке 
   (т.к. ввод на телефоне всеравно меняется немного от сенсора, даже если жмешь в одну точку)
   я хз как устроено InputAxis мож там и надо поддерживать ввод каждый кадр
    */
public class JoystickScript : MonoBehaviour
{

    public RectTransform PointerTransform;

    // точность
    public float precious = 0.2f;

    public float multiplier = 1.15f;

   

    // не помню зачем, но нужна
    bool isReadyToDrag;

    RectTransform rect;
    RectTransform Rect {
        get
        {
            if (rect == null)
                rect = GetComponent<RectTransform>();

            return rect;
        }
    }

    // для точности
    Vector2 prevDirection = new Vector2();




    //List<Touch> touchesToRemove = new List<Touch>();

    public void OnDrag(BaseEventData event_data)
    {
        // для получения точки касания
        PointerEventData ped = event_data as PointerEventData;

        Vector2 direction = ped.position - new Vector2(transform.position.x, transform.position.y);

        if (isReadyToDrag)
        {
            direction = Vector2.ClampMagnitude(direction, rect.rect.xMax);
            // Тут анимация кнопки джойстка
            PointerTransform.localPosition = direction;

            if (Math.Abs(direction.x - prevDirection.x) > precious || Math.Abs(direction.y - prevDirection.y) > precious)
            {
                prevDirection = direction;
                direction.y = Mathf.Clamp(direction.y * multiplier, -rect.rect.xMax, rect.rect.xMax);
                // Послать ввод (например direction.normalized)
                {
                    Avelog.Input.JoystickInput[0] = direction.x / rect.rect.xMax;
                    Avelog.Input.JoystickInput[1] = direction.y / rect.rect.yMax;
                }
            }

        }

    }

    public void ChangePos(Vector2 pos)
    {
        Rect.position = pos;
    }

    public void PointerDown(BaseEventData event_data)
    {
        FingerCheck(event_data);

        PointerEventData ped = event_data as PointerEventData;

        // вектор отклонения
        Vector2 direction = ped.position - new Vector2(transform.position.x, transform.position.y);

		direction = Vector2.ClampMagnitude (direction, rect.rect.xMax);

		// Тут анимация кнопки джойстка
		PointerTransform.localPosition = new Vector3(direction.x, direction.y);
		direction.y = Mathf.Clamp (direction.y * multiplier, -rect.rect.xMax, rect.rect.xMax);

        // Послать ввод (например direction.normalized)
        Avelog.Input.JoystickInput[0] = direction.x / rect.rect.xMax;
        Avelog.Input.JoystickInput[1] = direction.y / rect.rect.yMax;

        isReadyToDrag = true;
    }

    public void PointerUp(BaseEventData event_data)
    {
        Avelog.Input.isMouseOverJoystick = false;

        PointerTransform.localPosition = new Vector3(0, 0);

        // ПОСЛАТЬ ВВОД
        Avelog.Input.JoystickInput[0] = 0f;
        Avelog.Input.JoystickInput[1] = 0f;

        isReadyToDrag = false;

        PointerEventData ped = event_data as PointerEventData;
        //Avelog.Input.BlockedFingers.Remove(Input.GetTouch(ped.pointerId).fingerId);
        Avelog.Input.BlockedFingers.Clear();

    }
    
    public void FingerCheck(BaseEventData event_data)
    {
        PointerEventData ped = event_data as PointerEventData;
        if (ped.pointerId != -1)
        {
            Touch t = Input.GetTouch(ped.pointerId);

            if (!Avelog.Input.BlockedFingers.Contains(t.fingerId))
                Avelog.Input.BlockedFingers.Add(t.fingerId);
        }
        Avelog.Input.isMouseOverJoystick = true;

    }

  


}
