﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchRotateScript : MonoBehaviour {

    [Range(0f, 1f)]
    public float mouseHorSens = 1f;
    [Range(0f, 1f)]
    public float mouseVertSens = 0.5f;

    [Range(0f, 2f)]
    public float touchHorSens = 1f;
    [Range(0f, 2f)]
    public float touchVertSens = 0.5f;

    public AnimationCurve sensBySpeed = AnimationCurve.EaseInOut(0f, 0f, 10f, 1f);

    Vector3 prevPointerPos;
    bool isMouseButtonDown = false;
    bool IsMouseOverJoystick
    {
        get
        {
            return Avelog.Input.isMouseOverJoystick;
        }
    }

    private void Update()
    {
        CalcInputDelta();
    }

    void CalcInputDelta()
    {
        Vector3 inputDelta = Vector3.zero;
        int permittedTouchIndex = -1;
        for (int i = 0; i < Input.touchCount; ++i)
        {
            Touch tempTouch = Input.GetTouch(i);
            bool isTouchPermitted = true;
            for (int j = 0; j < Avelog.Input.BlockedFingers.Count; ++j)
            {
                if (Avelog.Input.BlockedFingers[j] == tempTouch.fingerId)
                {
                    isTouchPermitted = false;
                    break;
                }
            }

            if (isTouchPermitted)
            {
                permittedTouchIndex = i;
                break;
            }

        }

        if (permittedTouchIndex != -1)
        {
            Touch t = Input.GetTouch(permittedTouchIndex);
            inputDelta = new Vector3(t.deltaPosition.x * touchHorSens, t.deltaPosition.y * touchVertSens, 0f);
            float curSensSpeed = sensBySpeed.Evaluate(inputDelta.magnitude);
            inputDelta *= curSensSpeed;
        }

//#if UNITY_EDITOR
        // Поворот головы мышью (вместо тача)
        if (Input.GetMouseButtonDown(0))
        {
            prevPointerPos = Input.mousePosition;
            isMouseButtonDown = true;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isMouseButtonDown = false;
        }

        if (isMouseButtonDown && !IsMouseOverJoystick)
        {
            inputDelta += Input.mousePosition - prevPointerPos;
            inputDelta = new Vector3(inputDelta.x * mouseHorSens, inputDelta.y * mouseVertSens, 0f);

            if (prevPointerPos != Input.mousePosition)
                prevPointerPos = Input.mousePosition;
        }
//#endif

        Avelog.Input.touchDeltaX = inputDelta.x;
        Avelog.Input.touchDeltaY = inputDelta.y;

    }

}
